<?php
    require_once "_core/handles/dbhandle.php";
    require_once "_core/handles/userhandle.php";
    require_once "_core/pages/renderers/mainpage/mp_renderer.php";
    $dbf = new dbhandle();
    $uf = new userhandle();

    $loggedin = $uf->isLoggedIn();

    $pageNumber = isset($_GET['page']) ? $_GET['page'] - 1 : 0;
    if($pageNumber < 0) $pageNumber = 0;
    if(isset($_GET['post'])) $pageNumber = -1;

    $recentsearches = $dbf->queryToAssoc("SELECT * FROM searches_new WHERE RSN NOT IN (SELECT RSN FROM users WHERE HideRSN = 1) ORDER BY LastSearchDate DESC LIMIT 10");
    $recentposts = $dbf->queryToAssoc("SELECT fp.ThreadID, fp.PostDate, ft.Title, u.Username, u.RSN FROM forumposts fp JOIN forumthreads ft ON ft.ThreadID = fp.ThreadID JOIN users u ON u.UserID = fp.UserID WHERE IsDeleted=0 ORDER BY PostDate DESC LIMIT 5");
    if($pageNumber != -1) {
        $recentnews = $dbf->queryToAssoc("SELECT * FROM posts WHERE Visible=1 ORDER BY Sticky DESC, Date DESC");
    } else {
        $recentnews = $dbf->queryToAssoc("SELECT * FROM posts WHERE PostID='" . $dbf->db->real_escape_string($_GET['post']) . "'");
    }

    function time_elapsed_string($ptime) {
        $etime = time() - $ptime;

        if($etime < 1) {
            return '0 seconds';
        }

        $a = array(
            12 * 30 * 24 * 60 * 60 => 'year', 30 * 24 * 60 * 60 => 'month', 24 * 60 * 60 => 'day', 60 * 60 => 'hour', 60 => 'minute', 1 => 'second'
        );

        foreach($a as $secs => $str) {
            $d = $etime / $secs;
            if($d >= 1) {
                $r = round($d);
                return $r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago';
            }
        }
    }

?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Home - Maxcape</title>
        <link rel="stylesheet" href="_assets/css/corecsswrapper_light.css"/>
        <link rel="stylesheet" href="_assets/css/pages/mainpage.css"/>

        <?php if(!$loggedin): ?>
            <link rel="stylesheet" href="_assets/css/pages/loginregister.css"/>
        <?php endif; ?>

        <script src="_assets/js/jquery.js"></script>
        <script src="_assets/js/scripts/lavalamp.js"></script>
    </head>
    <body>
        <?php require_once "_core/pages/pageheader.php"; ?>

        <div class="pagewidth">

            <div id="mainContent">
                <?php
                    if(isset($_POST['errorCode']) && $_POST['errorCode'] != "") {
                        if($_POST['errorCode'] == "badrsn") {
                            ?>
                            <div class="errorMessage">
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <span><b>Error:</b> The name '<b><?php echo $_POST['errorData']; ?></b>' does not appear in the hiscores.</span>
                                <i class="fa fa-times-circle" aria-hidden="true" id="closeAlert"></i>
                            </div>
                        <?php
                        }
                    }
                ?>

                <?php
                    if($pageNumber != -1) {
                        mp_renderer::print_news_items($recentnews, $pageNumber, $dbf);
                    } else {
                        mp_renderer::print_news_items($recentnews, $pageNumber, $dbf);
                    }
                ?>

                <?php
                    if($pageNumber != -1) {
                        ?>
                        <div id="mp_pagination">
                            <?php
                                $totalPosts = $dbf->queryToText("SELECT COUNT(*) FROM posts WHERE Visible=1");
                                $totalPages = ceil(intval($totalPosts, 10) / mp_renderer::MAX_PER_PAGE);

                                for($i = 1; $i <= $totalPages; $i++) {
                                    ?>
                                    <div class="page_number <?php if($i - 1 == $pageNumber) {
                                        echo "active_page";
                                    } ?>">
                                        <span><a href="?page=<?= $i; ?>"><?= $i; ?></a></span>
                                    </div>
                                <?php
                                }
                            ?>
                        </div>
                    <?php
                    }
                ?>
            </div>
            <div id="sidebar">
                <div id="recentSearches">
                    <h2 class="categoryheader">Recent Searches</h2>
                    <ul class="sidebarlist">
                        <?php mp_renderer::print_recent_searches($recentsearches); ?>
                    </ul>
                </div>
                <div id="recentForumPosts">
                    <h2 class="categoryheader">Recent Forum Posts</h2>
                    <ul class="sidebarlist">
                        <?php mp_renderer::print_recent_forum_posts($recentposts); ?>
                    </ul>
                </div>

                <div id="donate">
                    <h2 class="categoryheader">Donate</h2>

                    <p>Donations go towards development and hosting of Maxcape. Maxcape is provided ad-free in order to provide the best possible experience.</p>

                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                        <input type="hidden" name="cmd" value="_s-xclick">
                        <input type="hidden" name="hosted_button_id"
                               value="MLGB9DZPENU6Y">
                        <input type="image"
                               src="http://www.maxcape.com/images/paypal.png"
                               name="submit"
                               alt="PayPal - The safer, easier way to pay online!">
                    </form>
                </div>
            </div>
        </div>

        <?php require_once "_core/pages/footer.php"; ?>
    </body>
</html>