<?php
    require_once "../../_core/handles/dbhandle.php";
    require_once "../..//_core/handles/userhandle.php";
    $dbf = new dbhandle();
    $uf = new userhandle();

?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Login</title>
        <link rel="stylesheet" href="/_assets/css/corecsswrapper_light.css"/>
    </head>
    <body>
        <?php require_once "../../_core/pages/pageheader.php"; ?>

        <div class="pagewidth">
            <div id="maincontent">
                <h1 class="fancyheader">Login</h1>

            </div>
        </div>
    </body>
</html>