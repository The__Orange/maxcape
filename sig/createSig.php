<?php
    header("Content-Type: image/png");

    function getRGB($hex) {
        $rgb = str_split($hex, 2);

        $r = hexdec($rgb[0]);
        $g = hexdec($rgb[1]);
        $b = hexdec($rgb[2]);

        return array($r, $g, $b);
    }

    function getStats( $rsn ) {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, "http://hiscore.runescape.com/index_lite.ws?player=" . $rsn );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

        $data = curl_exec( $ch );
        curl_close( $ch );

        return $data;
    }

    function createSignature() { //($rsn, $goal, $totalXP, $goalXP) {
        $background = imagecreatefrompng("base-with-bg.png");
        $bar = imagecreatefrompng("bar-center.png");
        $barEnd = imagecreatefrompng("bar-right.png");
        $cape = imagecreatefrompng("milestones/max.png");

        imagealphablending($background, true);
        imagesavealpha( $background, true );

        $percent = 0.38;
        $barY = 19;
        $barX = 69;
        for($i = $barX; $i <= floor(170 * $percent) + 69; $i++) {
            imagecopy($background, $bar, $i, $barY, 0, 0, imagesx($bar), imagesy($bar));
            $barX = $i;
        }

        imagecopy($background, $barEnd, $barX, $barY, 0, 0, imagesx($barEnd), imagesy($barEnd));
        imagecopyresampled($background, $cape, 24, 18, 0, 0, 25, 33, imagesx($cape), imagesy($cape));

        $font = "./OpenSans-Semibold.ttf";
        $fontSize = 20;
        $text = "38%";

        $textDim = imagettfbbox($fontSize, 0, $font, $text);
        $textX = $textDim[2] - $textDim[0];
        $textY = $textDim[7] - $textDim[1];

        $text_posX = (302/2) - ($textX / 2);
        $text_posY = (65/2) - ($textY / 2);



        $rsn = $_GET['rsn'];

        imagettftext($background, $fontSize, 0, $text_posX+1, $text_posY+1, imagecolorallocate($background, 0, 0, 0), $font, $text);
        imagettftext($background, $fontSize, 0, $text_posX, $text_posY, imagecolorallocate($background, 255,255,255), $font, $text);
        imagettftext($background, 12, 0, 58, 71, imagecolorallocate($background, 0  ,0  ,0  ), "./OpenSans-SemiBold.ttf", $rsn);
        imagettftext($background, 12, 0, 57, 70, imagecolorallocate($background, 255,255,255), "./OpenSans-SemiBold.ttf", $rsn);
        imagettftext($background, 8, 0, 61, 11, imagecolorallocate($background, 0  ,0  ,0  ), "./OpenSans-Regular.ttf", "123,456,789 XP Remaining");
        imagettftext($background, 8, 0, 60, 10, imagecolorallocate($background, 255,255,255), "./OpenSans-Regular.ttf", "123,456,789 XP Remaining");


        return imagepng($background);
    }

    echo createSignature();