<div id="pageFooter">
    <div class="pagewidth">
        <div id="copyrightinfo">
            <p>Maxcape &copy; The Orange 2012 - <?php echo date("Y"); ?></p>

            <p>RuneScape &copy; Jagex LTD 1999 - <?php echo date("Y"); ?></p>
        </div>
        <div id="social">
            <div id="socialicons">
                <div class="socialicon googleplus">
                    <div class="bannertop"></div>
                    <div class="bannermid"></div>
                    <div class="bannercontent"><i class="fa fa-google-plus"></i></div>
                    <div class="bannerbottom"></div>
                </div>

                <div class="socialicon facebook">
                    <div class="bannertop"></div>
                    <div class="bannermid"></div>
                    <div class="bannercontent"><i class="fa fa-facebook"></i></div>
                    <div class="bannerbottom"></div>
                </div>

                <div class="socialicon tumblr">
                    <div class="bannertop"></div>
                    <div class="bannermid"></div>
                    <div class="bannercontent"><i class="fa fa-tumblr"></i></div>
                    <div class="bannerbottom"></div>
                </div>

                <div class="socialicon twitter">
                    <div class="bannertop"></div>
                    <div class="bannermid"></div>
                    <div class="bannercontent"><i class="fa fa-twitter"></i></div>
                    <div class="bannerbottom"></div>
                </div>
            </div>
        </div>
    </div>
</div>