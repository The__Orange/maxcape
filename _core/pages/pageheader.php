<header>
    <div id="header">
        <div id="headerinner">
            <!--            <img src="/_assets/images/TL_icon.png" class="icon" alt="logo"/>-->
            <div id="headerIcon"></div>

            <h2>Maxcape</h2>

            <?php
                if(isset($loggedin) && $loggedin) {
                    $userid = $_SESSION['user']['UserID'];
                    $username = $_SESSION['user']['Username'];
                    $rsn = $_SESSION['user']['RSN'];
                    ?>
                    <ul class="ddl_menu">
                        <li class="ddl_main" id="userdisplay">
                            <img class="ddl_user_avatar" src="/forums/getAvatar.php?rsn=<?php echo urlencode($rsn); ?>"/>

                            <div>
                                <span class="ddl_username"><?php echo $username; ?></span>
                                <span class="ddl_rsn"><?php echo $rsn; ?></span> <i class="fa fa-sort-desc"></i>
                            </div>
                            <ul>
                                <li><a href="/profile/<?php echo $username; ?>">My Profile</a>
                                </li>
                                <li><a href="/ucp/tab/0">User Control Panel</a>
                                </li>
                                <li><a href="/_core/scripts/logout.php">Logout</a>
                                </li>
                            </ul>
                        </li>
                    </ul>

                    <script src="/new/_assets/js/scripts/pageheader_useropts.js"></script>
                <?php
                } else {
                    ?>
                    <ul class="ddl_menu">
                        <li class="ddl_main">
                            <span class="ddl_myaccount"><a href="/account/login">Login</a></span>

<!--                            --><?php //require_once $_SERVER['DOCUMENT_ROOT'] . "_core/pages/renderers/login/loginform.html"; ?>
                        </li>
                    </ul>
                <?php
                }
            ?>

                        <div id="timer-container" data-toggled="false">
                            <div id="timer">
                                <span class="fa fa-clock-o"></span>

                                <div id="timer-toggle-container">
                                    <p id="timer-text">0:00</p>
                                </div>
                            </div>
                        </div>
        </div>
    </div>
    <nav>
        <div id="navinner">
            <ul>
                <li class="active"><a href="/">Home</a></li>
                <!--                <li id="calcSearch" data-expanded="false">-->
                <!--                    <a href="javascript:void(0);">Max/Completionist Cape Calculator</a>-->
                <!---->
                <!--                    <div id="calcSearchBox">-->
                <!--                        <form action="/calc/" onsubmit="(function(e) { e.preventDefault(); document.location =-->
                <!--                        '/calc/' + $('#name').val().replace(/ /g, '+'); })(event)">-->
                <!--                            <input name="name" id="name" type="text" placeholder="Input RSN"/>-->
                <!--                        </form>-->
                <!---->
                <!--                    </div>-->
                <!--                </li>-->
                <li><a href="/designer/">Designer</a></li>
                <li><a href="/sig/">Signatures</a></li>
                <li><a href="/logs/">Logs</a></li>
                <li><a href="/search/">Search Profiles</a></li>
                <li><a href="/forums/">Forums</a></li>

                <?php
                    if(isset($userlevel) && $userlevel >= 4) {
                        ?>
                        <li><a href="/admin/">Admin</a></li>
                    <?php
                    }
                ?>
            </ul>

            <div id="lavalamp" class=""></div>


            <!--            <div id="socialicons">-->
            <!--                <div class="socialicon googleplus">-->
            <!--                    <div class="bannertop"></div>-->
            <!--                    <div class="bannermid"></div>-->
            <!--                    <div class="bannercontent"><i class="fa fa-google-plus"></i></div>-->
            <!--                    <div class="bannerbottom"></div>-->
            <!--                </div>-->
            <!---->
            <!--                <div class="socialicon facebook">-->
            <!--                    <div class="bannertop"></div>-->
            <!--                    <div class="bannermid"></div>-->
            <!--                    <div class="bannercontent"><i class="fa fa-facebook"></i></div>-->
            <!--                    <div class="bannerbottom"></div>-->
            <!--                </div>-->
            <!---->
            <!--                <div class="socialicon tumblr">-->
            <!--                    <div class="bannertop"></div>-->
            <!--                    <div class="bannermid"></div>-->
            <!--                    <div class="bannercontent"><i class="fa fa-tumblr"></i></div>-->
            <!--                    <div class="bannerbottom"></div>-->
            <!--                </div>-->
            <!---->
            <!--                <div class="socialicon twitter">-->
            <!--                    <div class="bannertop"></div>-->
            <!--                    <div class="bannermid"></div>-->
            <!--                    <div class="bannercontent"><i class="fa fa-twitter"></i></div>-->
            <!--                    <div class="bannerbottom"></div>-->
            <!--                </div>-->
            <!--            </div>-->

            <div class="container-1">
                <form action="#" onsubmit="(function(e) { e.preventDefault(); if($('#search').val() != '') { document.location = '/calc/' + $('#search').val().replace(/ /g, '+'); } })(event)">
                    <label for="search" class="icon"><i class="fa fa-search"></i></label>
                    <input type="search" id="search" placeholder="Search..." required="required" pattern="^[a-zA-Z0-9_\- ]*$"/>
                    <!--                <button type="submit">test</button>-->
                </form>
<!--                <div id="searchtype_switcher">-->
<!--                    <div id="searchtype_rs3" class="rslogo rslogoactive">-->
<!--                        <img src="/_assets/images/logo_rs3.png" alt="rs3"/>-->
<!--                    </div>-->
<!--                    <div id="searchtype_osrs" class="rslogo">-->
<!--                        <img src="/_assets/images/logo_osrs.png" alt="osrs"/>-->
<!--                    </div>-->
<!--                </div>-->
            </div
        </div>
    </nav>
</header>