<?php

    class mp_renderer {
        const MAX_PER_PAGE = 5;

        static function print_news_items($newslist, $pagenum, dbhandle $dbf) {
            foreach($newslist as $i => $news) {
                if(($i >= ($pagenum * self::MAX_PER_PAGE) && $i < (($pagenum * self::MAX_PER_PAGE) + self::MAX_PER_PAGE)) || $pagenum == -1) {
                    ?>
                    <div class="newsItem">
                        <h3 class='hdr'><a href='/?post=<?= $news['PostID']; ?>'><?= $news['Headline']; ?></a></h3>

                        <?= str_replace("\\", "", $news['Content']); ?>

                        <span class="newsFooter3dDummy"></span>
                        <?php if($pagenum != -1): ?>
                            <div class="newsFooter">
                                <span class="postedBy"><?php if($news['Sticky'] == 1): ?><i class="fa fa-thumb-tack"></i><?php endif; ?> Posted <?= date("F d, Y", strtotime($news['Date'])); ?> by Evan</span>
                                <span class="commentCount"><a href="?post=<?php echo $news['PostID']; ?>"><?= $dbf->queryToText("SELECT COUNT(*) FROM comments WHERE PostID='" . $news['PostID'] . "'"); ?> Comments</a></span>
                            </div>
                        <?php else: ?>
                            <div class="newsFooter">
                                <h3 class='hdr'>Comments</h3>

                                <div id="commentContainer">
                                    <?php mp_renderer::print_news_item_comments($news['PostID'], $dbf); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php
                }
            }
        }

        static function print_recent_searches($searchlist) {
            foreach($searchlist as $search) {
                ?>
                <li>
                    <div class="userportrait up_ex_sm">
                        <img src="/_core/getAvatar.php?rsn=<?= urlencode($search['RSN']); ?>" alt="Avatar"/>
                    </div>
                    <div class="postdetails">
                        <p class="userandtime searchHistory">
                            <a href="/calc/<?php echo urlencode($search['RSN']); ?>" class="user"><?= $search['RSN'] ?></a> <span><?= time_elapsed_string(strtotime($search['LastSearchDate'])); ?></span></p>
                    </div>
                </li>
            <?php
            }
        }

        static function print_recent_forum_posts($postlist) {
            foreach($postlist as $post) {
                ?>
                <li>
                    <div class="userportrait up_sm">
                        <img src="http://maxcape.com/forums/getAvatar.php?rsn=<?= urlencode($post['RSN']); ?>" onerror="this.src='avatar.png';" alt="Avatar"/>
                    </div>
                    <div class="postdetails">
                        <p class="threadtitle">
                            <span><a data-title="<?= $post['Title']; ?>" data-loop="0" href="/forums/t/<?= $post['ThreadID']; ?>"><?= $post['Title']; ?></a></span>
                        </p>

                        <p class="userandtime">
                            <a href="#" class="user"><?= $post['Username'] ?></a> <?= time_elapsed_string(strtotime($post['PostDate'])); ?></p>
                    </div>
                </li>
            <?php
            }
        }

        static function print_news_item_comments($newsID, dbhandle $dbf) {
            $commentlist = $dbf->queryToAssoc("SELECT * FROM comments c JOIN users u ON u.UserID = c.UserID WHERE PostID='$newsID' AND ReplyID IS NULL");
            foreach($commentlist as $comment) {
                $topbarcolor = $comment['CommentBGColor'];
                if(hexdec($topbarcolor) > 0xFFFFFF * 0.666) {
                    $colorClass = "darkText";
                } else if(hexdec($topbarcolor) > 0xFFFFFF * 0.5) {
                    $colorClass = "medText";
                } else {
                    $colorClass = "lightText";
                }
                ?>
                <div class="comment" id="comment-<?= $comment['CommentID']; ?>">
                    <div class="commentheader <?= $colorClass; ?>" style="background-color:<?= "#" . $comment['CommentBGColor']; ?>">
                        <img class="avatar" src="http://www.maxcape.com/forums/getAvatar.php?rsn=<?= urlencode($comment['RSN']); ?>" alt="Avatar"/>
                        <span><?= $comment['Username']; ?></span>
                        <span class="commentdate"><?= time_elapsed_string(strtotime($comment['PostDate'])); ?></span>
                    </div>
                    <div class="commentbody">
                        <?= $comment['Content']; ?>
                    </div>
                </div>
                <?php
                $replylist = $dbf->queryToAssoc("SELECT * FROM comments c JOIN users u on u.UserID = c.UserID WHERE PostID='$newsID' AND ReplyID='" . $comment['CommentID'] . "'");

                if(count($replylist) > 0) {
                    ?>
                    <div class="replies">
                        <?php
                            foreach($replylist as $reply):
                                ?>
                                <div class="comment" id="comment-<?= $reply['CommentID']; ?>">
                                    <div class="commentheader" style="background-color:<?= "#" . $reply['CommentBGColor']; ?>">
                                        <img class="avatar" src="http://www.maxcape.com/forums/getAvatar.php?rsn=<?= urlencode($reply['RSN']); ?>" alt="Avatar"/>
                                        <span><?= $reply['Username']; ?></span>
                                        <span class="commentdate"><?= time_elapsed_string(strtotime($comment['PostDate'])); ?></span>

                                    </div>
                                    <div class="commentbody">
                                        <?= $reply['Content']; ?>
                                    </div>
                                </div>
                            <?php
                            endforeach;
                        ?>
                    </div>
                <?php
                }
            }
        }
    }