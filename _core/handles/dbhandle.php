<?php
    require "core.php";
    class dbhandle {
        //Private connection variables
        private $dbserver   = 'localhost';
        private $dbuser     = 'root';
        private $dbpassword = '';
        private $dbdatabase = 'mcc';
        private $init       = true;

        //Holds the database link
        public $db;

        function __construct() {
            //Set up database link
            //Shut up any errors with @new to handle below
            $this->db = @new mysqli($this->dbserver, $this->dbuser, $this->dbpassword, $this->dbdatabase);
            if($this->db->connect_error) {
                //Dummy variable to prevent attempting to close non-existant connection
                $this->init = false;
                die("Failed to connect to Database: (Error " . $this->db->connect_errno . ") " . $this->db->connect_error);
            }
        }

        function __destruct() {
            //Close database connection on destruction
            if($this->init) $this->db->close();
        }

        function getErr() {
            //Get last query error
            return $this->db->error;
        }

        function getInsertID() {
            //Get insert id for last query
            return $this->db->insert_id;
        }

        function sterilize($string) {
            //sterilize user input
            return $this->db->real_escape_string($string);
        }

        public function query($query) {
            //Execute a query
            return $this->db->query($query);
        }

        function queryToAssoc($query) {
            $array = array();
            $res = $this->query($query);
            while($row = $res->fetch_assoc()) {
                $array[] = $row;
            }

            return $array;
        }

        function queryToText($query) {
            $res = $this->query($query);
            return $res->fetch_array()[0];
        }



    }