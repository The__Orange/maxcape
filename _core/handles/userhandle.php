<?php
    /*
     * TODO:
     *    - Remove RSN from users table to support multiple RSN's per account
     */

    session_start();
    require_once "dbhandle.php";
    class userhandle extends dbhandle {
        const SECONDS_PER_DAY = 86400;
        const COOKIE_DURATION = 14; //14 days

        private $password_regex = "/.{5,}/"; //Allow any character, minimum length 5
        private $username_regex = "/^[a-zA-Z0-9_\- ]{1,20}$/"; //Allow a-z (lower & caps), 0 - 9, underscore, hyphen, 1 char min, 12 char max

        private $user_data = array(
            "UserID" => "",
            "Username" => "",
            "RSN" => ""
        );

        private function generateLoginKey() {
            //Token used to authorize a cookie
            return uniqid('', false);
        }

        function isLoggedIn() {
            if(isset($_SESSION['user'])) {
                if($_SESSION['user']['UserID'] != 0 && $_SESSION['user']['UserID'] != "") {
                    //An attempt to prevent a bug with unrecognized userid being set to 0 or null
                    return true;
                } else {
                    unset($_SESSION['user']);
                    return false;
                }
            } else if(isset($_COOKIE['user_token']) && isset($_COOKIE['user_name'])) {
                //Authorize user on set cookies
                return $this->authorizeUser();
            }

            return false;
        }

        function logoutUser() {
            if(isset($_SESSION['user'])) {
                unset($_SESSION['user']);
                setcookie("user_name", '', time() - $this::SECONDS_PER_DAY);
                setcookie("user_token", '', time() - $this::SECONDS_PER_DAY);
            }
        }

        private function authorizeUser($username = "", $loginKey = "") {
            if($loginKey == "" && $username == "") {
                //Both variables will only be blank if this is an automatic login from cookies.
                $loginKey = $this->db->real_escape_string($_COOKIE['user_token']);
                $username = $this->db->real_escape_string($_COOKIE['user_name']);
            }

            //If loginkey is still null, we aren't using a key, as this user won't be remembered with cookies
            if($loginKey == "") {
                $user = $this->queryToAssoc("SELECT UserID, Username, RSN FROM users WHERE Username='$username' AND LoginKey='$loginKey' LIMIT 1");
            } else {
                $user = $user = $this->queryToAssoc("SELECT UserID, Username, RSN FROM users WHERE Username='$username' LIMIT 1");
            }

            $user = $user[0];

            if($user) {
                //Set up user data
                $this->user_data['UserID'] = $user['UserID'];
                $this->user_data['Username'] = $user['Username'];
                $this->user_data['RSN'] = $user['RSN'];

                $_SESSION['user'] = $this->user_data;

                return true;
            } else {
                unset($_SESSION['user']);
                return false;
            }

        }

        function registerUser($RAW_username, $RAW_email, $RAW_password, $RAW_passwordConfirm, $rememberMe) {
            //Sanitize Variables
            $username = $this->db->real_escape_string($RAW_username);
            $email = $this->db->real_escape_string($RAW_email);
            $password = $this->db->real_escape_string($RAW_password);
            $passwordConfirm = $this->db->real_escape_string($RAW_passwordConfirm);

            //Check if username entered is valid
            if(preg_match($this->username_regex, $username)) {
                //Check if password is valid
                if(preg_match($this->password_regex, $password)) {
                    //Check if passwords match
                    if($password === $passwordConfirm) {
                        //Check if the provided email is valid
                        if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                            //Check if username is already taken
                            if(!$this->queryToAssoc("SELECT UserID FROM users WHERE Username='$username' LIMIT 1")) {
                                //Check if email is already used
                                if(!$this->queryToAssoc("SELECT UserID FROM users WHERE Email='$email' LIMIT 1")) {
                                    $hashedPassword = crypt($password);
                                    $this->query("INSERT INTO users (Username, Password, Email, JoinDate) VALUES ('$username', '$hashedPassword', '$email', NOW())");

                                    if($this->loginUser($username, $password, $rememberMe)) {
                                       return "success";
                                    } else return "err_login_failure";
                                } else return "err_email_in_use";
                            } else return "err_username_in_use";
                        } else return "err_invalid_email";
                    } else return "err_passwords_different";
                } else return "err_invalid_password";
            } else return "err_invalid_username";
        }

        function loginUser($RAW_username, $RAW_password, $rememberMe) {
            $username = $this->db->real_escape_string($RAW_username);
            $password = $this->db->real_escape_string($RAW_password);

            $info = $this->queryToAssoc("SELECT * FROM users WHERE Username='$username' LIMIT 1");
            $info = $info[0];

            //Check if user exists
            if($info) {
                //Verify passwords match
                if(crypt($password, $info['Password']) == $info['Password']) {
                    $userid = $info['UserID'];

                    if($rememberMe) {
                        //Set up login keys to validate users auto login cookies
                        if(strtotime("now") < strtotime($info['LoginKeyExpiration'])) {
                            $loginKey = $info['LoginKey'];
                        } else {
                            $loginKey = $this->generateLoginKey();
                        }

                        //Expire the login key at the same time the cookie expires
                        $loginKeyExpiration = strtotime("now + " . $this::COOKIE_DURATION . " days");
                        $this->query("UPDATE users SET LoginKey='$loginKey', LoginKeyExpiration='$loginKeyExpiration' WHERE UserID='$userid' LIMIT 1");

                        //Set cookies
                        setcookie("user_name", $username, time() + ($this::COOKIE_DURATION * $this::SECONDS_PER_DAY));
                        setcookie("user_token", $loginKey, time() + ($this::COOKIE_DURATION * $this::SECONDS_PER_DAY));

                        return $this->authorizeUser($username, $loginKey);
                    } else {
                        return $this->authorizeUser($username, "");
                    }
                } else return false;
            }
        }
    }