<?php
    date_default_timezone_set("America/Los_Angeles");

    function postErrorRedirect($error) {
        ?>

        <p><b>Error</b></p>
        <form method="post" action="/" id="errorRefresh">
            <input name="errorCode" id="errorCode" type="hidden" value="<?php echo $error['code']; ?>">
            <input name="errorData" id="errorData" type="hidden" value="<?php echo $error['data']; ?>"/>

            <button type="submit">Click here to return to the main page</button>
        </form>

        <script>
            document.forms['errorRefresh'].submit();
        </script>
    <?php
        die();
    }