<?php
    require_once "../handles/dbhandle.php";
    require_once "../handles/userhandle.php";
    $dbf = new dbhandle();
    $uf = new userhandle();

    $raw_username = $_GET['login_username'];
    $raw_password = $_GET['login_password'];
    $remember_me = isset($_GET['remember_me']);

    if($uf->loginUser($raw_username, $raw_password, $remember_me)) {
        header("Location: /#success");
    } else {
        header("Location: /#fail");
    }