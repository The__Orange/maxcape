<?php

    class stat {
        public $type;
        public $maxLevel;

        public $rank;
        public $level;
        public $experience;

        public function __construct($type, $maxlevel, $rank, $level, $experience) {
            $this->type       = $type;
            $this->maxLevel   = $maxlevel;
            $this->rank       = $rank;
            $this->level      = $level;
            $this->experience = $experience;
        }
    }

    class calc {
        private $stats;

        private $NORMALXPSCALE = [
            0, 83, 174, 276, 388, 512, 650, 801, 969, 1154, 1358, 1584, 1833, 2107, 2411, 2746, 3115, 3523, 3973, 4470, 5018, 5624, 6291,
            7028, 7842, 8740, 9730, 10824, 12031, 13363, 14833, 16456, 18247, 20224, 22406, 24815, 27473, 30408, 33648, 37224, 41171, 45529,
            50339, 55649, 61512, 67983, 75127, 83014, 91721, 101333, 111945, 123660, 136594, 150872, 166636, 184040, 203254, 224466, 247886,
            273742, 302288, 333804, 368599, 407015, 449428, 496254, 547953, 605032, 668051, 737627, 814445, 899257, 992895, 1096278, 1210421,
            1336443, 1475581, 1629200, 1798808, 1986068, 2192818, 2421087, 2673114, 2951373, 3258594, 3597792, 3972294, 4385776, 4842295, 5346332,
            5902831, 6517253, 7195629, 7944614, 8771558, 9684577, 10692629, 11805606, 13034431, 14391160, 15889109, 17542976, 19368992, 21385073,
            23611006, 26068632, 28782069, 31777943, 35085654, 38737661, 42769801, 47221641, 52136869, 57563718, 63555443, 70170840, 77474828, 85539082,
            94442737, 104273167, 115126838, 127110260, 140341028, 154948977, 171077457, 188884740, 200000001
        ];

        private $ELITEXPSCALE = [
            0, 830, 1861, 2902, 3980, 5126, 6390, 7787, 9400, 11275, 13605, 16372, 19656, 23546, 28138, 33520, 39809,
            47109, 55535, 64802, 77190, 90811, 106221, 123573, 143025, 164742, 188893, 215651, 245196, 277713, 316311, 358547, 404634,
            454796, 509259, 568254, 632019, 700797, 774834, 854383, 946227, 1044569, 1149696, 1261903, 1381488, 1508756, 1644015, 1787581,
            1939773, 2100917, 2283490, 2476369, 2679907, 2894505, 3120508, 3358307, 3608290, 3870846, 4146374, 4435275, 4758122, 5096111,
            5449685, 5819299, 6205407, 6608473, 7028694, 7467354, 7924122, 8399751, 8925664, 9472665, 10041285, 10632061, 11245538, 11882262,
            12542789, 13227679, 13937496, 14672812, 15478994, 16313404, 17176661, 18069395, 18992239, 19945843, 20930821, 21947856, 22997593,
            24080695, 25259906, 26475754, 27728955, 29020233, 30350318, 31719944, 33129852, 34580790, 36073511, 37608773, 39270442, 40978509,
            42733789, 44537107, 46389292, 48291180, 50243611, 52247435, 54303504, 56412678, 58575823, 60793812, 63067521, 65397835, 67785643,
            70231841, 72737330, 75303019, 77929820, 80618654, 83370455, 86186124, 89066630, 92012904, 95025896, 98106559, 101255855, 104474750,
            107764216, 111125230, 114558777, 118065845, 121647430, 125304532, 129038159, 132849323, 136739041, 140708338, 144758242, 148889790,
            153104021, 157401983, 161784728, 166253312, 170808801, 175452262, 180184770, 185007406, 189921255, 19492740, 200000001
        ];

        public function __construct($rsn, $skillList) {
            $this->stats = $this->parseStats($this->getStats($rsn), $skillList);
        }

        public function getStat($stat) {
            return $this->stats[$stat];
        }

        private function getStats($rsn) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://hiscore.runescape.com/index_lite.ws?player=" . $rsn);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            $data = curl_exec($ch);

            if(curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200) {
                curl_close($ch);
                return $data;
            } else {
                curl_close($ch);
                $error = [
                    'code' => 'badrsn',
                    'data' => $rsn
                ];

                postErrorRedirect($error);
            }
        }

        private function parseStats($stats, $skills) {
            $playerStats = [];

            $stats = preg_split("/\s+/", $stats);
            for($i = 0; $i < count($skills); $i++) {
                $thisStat   = explode(",", $stats[$i]);
                $rank       = $thisStat[0] == -1 ? 0 : $thisStat[0];
                $level      = $thisStat[1] == 0 ? 1 : $thisStat[1];
                $experience = $thisStat[2] == -1 ? 0 : $thisStat[2];

                $playerStats[$skills[$i]["Name"]] = new stat($skills[$i]["SkillType"], $skills[$i]["MaxLevel"], $rank, $level, $experience);


            }

            return $playerStats;
        }

        private function getExperience($level, $skillType) {
            $xpScale = $this->getXPScale($skillType);

            return $xpScale[$level - 1];
        }

        private function getXPScale($type) {
            return $type == "Normal" ? $this->NORMALXPSCALE : $this->ELITEXPSCALE;
        }

        public function getVirtualLevel($skill) {
            $skill   = $this->stats[$skill];
            $exp     = $skill->experience;
            $xpScale = $this->getXPScale($skill->type);

            for($i = 0; $i < count($xpScale); $i++) {
                if($exp >= $xpScale[$i] && $exp < $xpScale[$i + 1]) {
                    return $i + 1;
                }
            }
            return 1;
        }

        public function calculateExperienceRemaining($skill, $targetLevel = 0) {
            $stat = $this->stats[$skill];

            if($stat->experience == 200000000) {
                return 0;
            }

            if($targetLevel == 0) {
                if($this->getVirtualLevel($skill) < count($this->getXPScale($stat->type))) {
                    $targetLevel = $this->getVirtualLevel($skill) + 1;
                } else {
                    return 0;
                }
            } else if($stat->level >= $targetLevel) {
                return 0;
            }
            $targetXP = $this->getExperience($targetLevel, $stat->type);

            if($targetXP == 200000001) {
                $targetXP = 200000000;
            }
            return $targetXP - $stat->experience;
        }

        public function calculateCombatLevel() {
            return floor(0.25 * (1.3 * max($this->getStat("Attack")->level + $this->getStat("Strength")->level, 2 * $this->getStat("Magic")->level, 2 * $this->getStat("Ranged")->level) + $this->getStat("Defence")->level + $this->getStat("Constitution")->level + (0.5 * $this->getStat("Prayer")->level) + (0.5 * $this->getStat("Summoning")->level)));
        }


        public function calculateSkillProgress($skill, $goalLevel) {
            $stat   = $this->stats[$skill];
            $goalXP = $this->getExperience($goalLevel, $stat->type);

            if($stat->experience != "?") {
                if($stat->experience >= $goalXP) {
                    return 100;
                } else {
                    return number_format(($stat->experience / $goalXP) * 100, 2);
                }
            } else {
                return 0;
            }

        }

        public function calculateAchievedMilestone() {
            foreach($this->stats as $stat) {

            }
        }

        public function calculateTotalProgress($milestone) {
            $totalReqExp = 0;
            $totalExp    = 0;

            if($milestone >= 1 && $milestone <= 120) {
                foreach($this->stats as $skill => $stat) {
                    if($skill != "Overall") {
                        $goalXP = $this->getExperience($milestone, $stat->type);
                        $totalReqExp += $goalXP;

                        if($stat->experience < $goalXP) {
                            $totalExp += $stat->experience;
                        } else {
                            $totalExp += $goalXP;
                        }
                    }
                }
            } else if($milestone == "comp") {
                foreach($this->stats as $skill => $stat) {
                    if($skill != "Overall") {
                        $goalXP = $this->getExperience($stat->maxLevel, $stat->type);
                        $totalReqExp += $goalXP;

                        if($stat['exp'] < $goalXP) {
                            $totalExp += $stat->experience;
                        } else {
                            $totalExp += $goalXP;
                        }
                    }
                }
            }

            return [
                "totalReqExp"    => $totalReqExp,
                "totalGainedExp" => $totalExp,
                "expRemaining"   => $totalReqExp - $totalExp,
                "percentage"     => number_format(((($totalReqExp - ($totalReqExp - $totalExp)) / $totalReqExp) * 100), 2)
            ];
        }
    }