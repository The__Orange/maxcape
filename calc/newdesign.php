<?php
    require_once "../_core/scripts/calc.php";
    require_once "../_core/handles/dbhandle.php";
    $dbf = new dbhandle();

    $rsn = $dbf->sterilize($_GET['name']);
    $skills = $dbf->queryToAssoc("SELECT * FROM skills");

    $calc = new calc($rsn, $skills);

    $dbf->query("INSERT INTO searches_new (RSN, TimesSearched, LastSearchDate) VALUES ('$rsn', 1, NOW()) ON DUPLICATE KEY UPDATE TimesSearched=TimesSearched + 1, LastSearchDate = NOW()" );
    $dbf->query("INSERT INTO searches (RSN, Time) VALUES ('$rsn', NOW())");

    $target = 99;
    $totalProgress = $calc->calculateTotalProgress($target);
?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
        <link rel="stylesheet" href="/_assets/css/corecsswrapper_light.css"/>
        <link rel="stylesheet" href="/_assets/css/pages/calc.css"/>
        <link rel="stylesheet" href="/_assets/css/pages/calc_badges.css"/>
        <script src="/_assets/js/jquery.js"></script>
        <script src="/_assets/js/scripts/lavalamp.js"></script>
        <script src="/_assets/js/circle-progress.js"></script>

        <script>
            $(document).ready(function () {
                $(".demo-1").circleProgress({
                    value: <?php echo $totalProgress["percentage"] / 100 ?>,
                    size: 200,
                    fill: {
                        color: "#D74E5E"
                    },
                    emptyFill: "#D3D3D3",
                    animation: {
                        duration: 500
                    }
                });
            });
        </script>
        <style>

        </style>
    </head>
    <body>
        <?php require_once "../_core/pages/pageheader.php"; ?>

        <div class="pagewidth">
            <div id="calcHeader">
                <div id="player" class="headerModule">

                    <h3 class="sidebarHeader">Player Information</h3>

                    <div class="headerModule_inner">
                        <div id="avatar">
                            <div class="demo-1"></div>
                            <img src="../_core/getAvatar.php?rsn=<?php
                                echo urlencode($rsn);
                            ?>"/>

                            <p><?php
                                    echo $totalProgress["percentage"] >= 100 ? 100 : $totalProgress["percentage"];
                                ?>%</p>
                        </div>
                        <div id="userinformation">
                            <h2><?php
                                    echo $rsn;
                                ?></h2>

                            <div style="display:inline-block;">
                                <div id="statinfo">
                                    <div class="stat" id="experienceInfo">
                                        <div class="userinfoiconnew exp" title="Total Experience"></div>
                                        <p><?php
                                                echo number_format($calc->getStat('Overall')->experience);
                                            ?></p>
                                    </div>
                                    <div class="stat">
                                        <div class="userinfoiconnew totallevel" title="Total Level"></div>
                                        <p><?php
                                                echo number_format($calc->getStat('Overall')->level);
                                            ?></p>
                                    </div>
                                    <div class="stat">
                                        <div class="userinfoicon combatant" title="Combat Level"></div>
                                        <p><?php
                                                echo $calc->calculateCombatLevel();
                                            ?></p>
                                    </div>
                                </div>

                                <div class="stat xpremaining">
                                    <p>XP Remaining: <?php echo number_format($totalProgress["expRemaining"]); ?></p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="recalculateContainer" class="headerModule">
                    <h3 class="sidebarHeader">Target Levels</h3>

                    <div class="headerModule_inner">
                        <div id="quickCalculateContainer">
                            <label for="quickCalculate">Presets: </label>
                            <select id="quickCalculate">
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="40">40</option>
                                <option value="50">50</option>
                                <option value="60">60</option>
                                <option value="70">70</option>
                                <option value="80">80</option>
                                <option value="90">90</option>
                                <option value="questcape">Quest Cape</option>
                                <option value="99" selected="selected">Max Cape</option>
                                <option value="comp">Completionist Cape</option>
                                <option value="mstrquestcape">Master Quest Cape</option>
                                <option value="120">Skill Mastery (120)</option>
                                <option value="200000000">200m</option>
                            </select>
                        </div>
                        <div class="nofloat">

                            <form id="quickCalculateForm" onsubmit="calc.fn.recalculate();">
                                <?php
                                    foreach($skills as $skill) {
                                        $skillname = $skill["Name"];
                                        if($skillname != "Overall") {
                                            ?>
                                            <div class="skillTargetContainer" data-skill="<?php echo strtolower($skillname); ?>">
                                                <label for="skillTarget-<?php echo strtolower($skillname); ?>"><img src="../images/<?php echo $skillname; ?>.png" alt="<?php echo $skillname; ?>"></label>
                                                <input id="skillTarget-<?php echo strtolower($skillname); ?>" type="number" value="<?php echo $skillname != "Overall" ? $target : $target * count($skills); ?>" data-skill="<?php echo strtolower($skillname); ?>"/>
                                            </div>
                                        <?php
                                        }
                                    }
                                ?>
                                <div class="nofloat">
                                    <button type="submit">Calculate</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


                <div class="headerModule" id="badgeContainer">
                    <h3 class="sidebarHeader">Badges</h3>

                    <div class="headerModule_inner">
                        <div id="badges">
                            <div class="badge survivalist" data-badge="Survivalist" data-text="Level 99 in the following skills:<br><b>Agility</b>, <b>Hunter</b>, <b>Thieving</b>, and <b>Slayer</b>"></div>
                            <div class="badge naturalist unobtained" data-badge="Naturalist" data-text="Level 99 in the following skills:<br><b>Cooking</b>, <b>Farming</b>, <b>Herblore</b>, and <b>Runecrafting</b>."></div>
                            <div class="badge gatherer unobtained" data-badge="Gatherer" data-text="Level 99 in the following skills:<br><b>Woodcutting</b>, <b>Mining</b>, <b>Fishing</b>, and <b>Divination</b>."></div>
                            <div class="badge twentyfive" data-badge="Experienced" data-text="At least 25,000,000 experience in a skill."></div>
                            <div class="badge classic unobtained" data-badge="Classic Completionist" data-text="Level 99 in all skills available in RuneScape Classic."></div>
                            <div class="badge f2pMax unobtained" data-badge="F2P Maxed" data-text="Level 99 in all skills available in Free to Play RuneScape"></div>
                            <div class="badge onebil unobtained" data-badge="Billionaire" data-text="At least 1,000,000,000 total experience"></div>
                            <div class="badge lvl138" data-badge="Master of Combat" data-text="Achieved the maximum combat level of 138"></div>
                            <div class="badge portmaster" data-badge="Portmaster" data-text="Level 90+ in all skills required for Player Owned Ports"></div>
                            <div class="badge balanced" data-badge="Balanced" data-text="Average Combat and Skilling skill levels within 5% of eachother"></div>
                            <div class="badge scaper" data-badge="Scaper" data-text="At least 15 skills at 99"></div>
                            <div class="badge mastercape unobtained" data-badge="Skill Mastery" data-text="At least one true skill mastery cape, besides Dungeoneering. (virtual level 120)"></div>
                        </div>

                        <div id="badgeInfo">
                            <div class="arrowUp"></div>
                            <div class="badgeInfoBody"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="pageContent">
                <div id="calcSkillContainer" data-totalskills="<?php echo count($skills) - 1; ?>">
                    <div id="skillprogress-wrapper">
                        <div class="skillprogress skillprogressheader">
                            <span class="skill-text icon"><img usemap="#sortmap0" class="sortBtn" src="/_assets/images/updown-down.png"/></span>
                            <span class="skill-text skillname"><img usemap="#sortmap1" class="sortBtn" src="/_assets/images/updown.png"/>Skill</span>
                            <span class="skill-text skillrank"><img usemap="#sortmap2" class="sortBtn" src="/_assets/images/updown.png"/>Rank</span>
                            <span class="skill-text skilllevel"><img usemap="#sortmap3" class="sortBtn" src="/_assets/images/updown.png"/>Level</span>
                            <span class="skill-text skillexp"><img usemap="#sortmap4" class="sortBtn" src="/_assets/images/updown.png"/>Experience</span>
                            <span class="skill-text skilltnl"><img usemap="#sortmap5" class="sortBtn" src="/_assets/images/updown.png"/>To Next Level</span>
                            <span class="skill-text skillexpremaining"><img usemap="#sortmap6" class="sortBtn" src="/_assets/images/updown.png"/>To Target Level</span>
                            <span class="skill-text skillpercentage"><img usemap="#sortmap7" class="sortBtn" src="/_assets/images/updown.png"/>Progress</span>
                        </div>
                        <?php
                            foreach($skills as $skill) {
                                $skillname = $skill["Name"];
                                if($skillname != "Overall") {
                                    $stat = $calc->getStat($skillname);
                                    $virtualLevel = $calc->getVirtualLevel($skillname) + 1;
                                    if($virtualLevel == 127) {
                                        $virtualLevel = "MAX";
                                    } else if ($virtualLevel == 151) {
                                        $virtualLevel = "MAX";
                                    }
                                    ?>
                                    <div id="skill-<?php echo strtolower($skillname); ?>" class="skillprogress skill-<?php echo strtolower($skillname); ?>" data-skilltype="<?php echo $stat->type; ?>" data-maxlevel="<?php echo $stat->maxLevel; ?>" data-target="99">
                                        <!--                                        <div class="progress" style="width:--><?php //echo $calc->calculateSkillProgress($skillname, 99); ?><!--%"></div>-->
                                        <span class="skill-text icon"><img class="skill-icon" src="../images/<?php echo $skillname; ?>.png"></span>
                                        <span class="skill-text skillname"><?php echo $skillname; ?></span>
                                        <span class="skill-text skillrank"><?php echo $stat->rank != 0 ? number_format($stat->rank) : "Unranked"; ?></span>
                                        <span class="skill-text skilllevel"><?php echo $stat->level; ?></span>
                                        <span class="skill-text skillexp"><?php echo number_format($stat->experience); ?></span>
                                        <span class="skill-text skillexpremaining"><span class="levelDisplay <?php echo $virtualLevel == "MAX" ? "levelDisplayMax" : "" ?>"><?php echo $virtualLevel; ?></span><?php echo number_format($calc->calculateExperienceRemaining($skillname)) ?></span>
                                        <span class="skill-text skillexpremaining skilltarget"><span class="levelDisplay">99</span><span class="xpDisplay"><?php echo number_format($calc->calculateExperienceRemaining($skillname, $target)); ?></span></span>
                                        <span class="skill-text skillpercentage">
                                            <div class="skillbar_wrapper">
                                                <div class="skillbar_bar_maxwidth">
                                                    <div class="skillbar_bar" id="" style="width:<?php echo $calc->calculateSkillProgress($skillname, 99); ?>%"></div>
                                                </div>
                                                <div class="skill_bar_percent">
                                                    <?php echo $calc->calculateSkillProgress($skillname, 99); ?>%
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                <?php
                                }
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <?php require_once "../_core/pages/footer.php"; ?>

        <script src="/_assets/js/scripts/calc_recalculate.js"></script>

        <map name="sortmap0">
            <area shape="rect" coords="0,0,11,7" title="Sort Ascending" href="javascript:void(0)" onclick="calc.fn.sort(0, 0);">
            <area shape="rect" coords="1,7,11,15" title="Sort Descending" href="javascript:void(0)" onclick="calc.fn.sort(0, 1);">
        </map>
        <map name="sortmap1">
            <area shape="rect" coords="0,0,11,7" title="Sort Ascending" href="javascript:void(0)" onclick="calc.fn.sort(1, 0);">
            <area shape="rect" coords="1,7,11,15" title="Sort Descending" href="javascript:void(0)" onclick="calc.fn.sort(1, 1);">
        </map>
        <map name="sortmap2">
            <area shape="rect" coords="0,0,11,7" title="Sort Ascending" href="javascript:void(0)" onclick="calc.fn.sort(2, 0);">
            <area shape="rect" coords="1,7,11,15" title="Sort Descending" href="javascript:void(0)" onclick="calc.fn.sort(2, 1);">
        </map>
        <map name="sortmap3">
            <area shape="rect" coords="0,0,11,7" title="Sort Ascending" href="javascript:void(0)" onclick="calc.fn.sort(3, 0);">
            <area shape="rect" coords="1,7,11,15" title="Sort Descending" href="javascript:void(0)" onclick="calc.fn.sort(3, 1);">
        </map>
        <map name="sortmap4">
            <area shape="rect" coords="0,0,11,7" title="Sort Ascending" href="javascript:void(0)" onclick="calc.fn.sort(4, 0);">
            <area shape="rect" coords="1,7,11,15" title="Sort Descending" href="javascript:void(0)" onclick="calc.fn.sort(4, 1);">
        </map>
        <map name="sortmap5">
            <area shape="rect" coords="0,0,11,7" title="Sort Ascending" href="javascript:void(0)" onclick="calc.fn.sort(5, 0);">
            <area shape="rect" coords="1,7,11,15" title="Sort Descending" href="javascript:void(0)" onclick="calc.fn.sort(5, 1);">
        </map>
        <map name="sortmap6">
            <area shape="rect" coords="0,0,11,7" title="Sort Ascending" href="javascript:void(0)" onclick="calc.fn.sort(6, 0);">
            <area shape="rect" coords="1,7,11,15" title="Sort Descending" href="javascript:void(0)" onclick="calc.fn.sort(6, 1);">
        </map>
        <map name="sortmap7">
            <area shape="rect" coords="0,0,11,7" title="Sort Ascending" href="javascript:void(0)" onclick="calc.fn.sort(7, 0);">
            <area shape="rect" coords="1,7,11,15" title="Sort Descending" href="javascript:void(0)" onclick="calc.fn.sort(7, 1);">
        </map>

        <script src="/_assets/js/scripts/calc.js"></script>
    </body>
</html>