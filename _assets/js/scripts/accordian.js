(function ($) {
    $.fn.accordian = function (opts) {
        $(this).find("li").each(function () {
            var $li = $(this);

            $li.data('expanded', 'false');

            $li.bind("click", function() {
                if($(this).data("expanded") == "false") {
                    $(this).find("div").slideDown(100);
                    $(this).find("span").text("-");
                    $(this).data("expanded", "true");
                } else {
                    $(this).find("span").text("+");
                    $(this).find("div").slideUp(100);
                    $(this).data("expanded", "false");
                }
            });
        });
    }
})(jQuery);