function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

var calc;
calc = {
    settings: {
        sortCol: 0,
        sortOrder: 1
    },
    xpScales: {
        normal: [
            0, 83, 174, 276, 388, 512, 650, 801, 969, 1154, 1358, 1584, 1833, 2107, 2411, 2746, 3115, 3523, 3973, 4470, 5018, 5624, 6291,
            7028, 7842, 8740, 9730, 10824, 12031, 13363, 14833, 16456, 18247, 20224, 22406, 24815, 27473, 30408, 33648, 37224, 41171, 45529,
            50339, 55649, 61512, 67983, 75127, 83014, 91721, 101333, 111945, 123660, 136594, 150872, 166636, 184040, 203254, 224466, 247886,
            273742, 302288, 333804, 368599, 407015, 449428, 496254, 547953, 605032, 668051, 737627, 814445, 899257, 992895, 1096278, 1210421,
            1336443, 1475581, 1629200, 1798808, 1986068, 2192818, 2421087, 2673114, 2951373, 3258594, 3597792, 3972294, 4385776, 4842295, 5346332,
            5902831, 6517253, 7195629, 7944614, 8771558, 9684577, 10692629, 11805606, 13034431, 14391160, 15889109, 17542976, 19368992, 21385073,
            23611006, 26068632, 28782069, 31777943, 35085654, 38737661, 42769801, 47221641, 52136869, 57563718, 63555443, 70170840, 77474828, 85539082,
            94442737, 104273167, 115126838, 127110260, 140341028, 154948977, 171077457, 188884740, 200000001
        ],
        elite: [
            0, 830, 1861, 2902, 3980, 5126, 6390, 7787, 9400, 11275, 13605, 16372, 19656, 23546, 28138, 33520, 39809,
            47109, 55535, 64802, 77190, 90811, 106221, 123573, 143025, 164742, 188893, 215651, 245196, 277713, 316311, 358547, 404634,
            454796, 509259, 568254, 632019, 700797, 774834, 854383, 946227, 1044569, 1149696, 1261903, 1381488, 1508756, 1644015, 1787581,
            1939773, 2100917, 2283490, 2476369, 2679907, 2894505, 3120508, 3358307, 3608290, 3870846, 4146374, 4435275, 4758122, 5096111,
            5449685, 5819299, 6205407, 6608473, 7028694, 7467354, 7924122, 8399751, 8925664, 9472665, 10041285, 10632061, 11245538, 11882262,
            12542789, 13227679, 13937496, 14672812, 15478994, 16313404, 17176661, 18069395, 18992239, 19945843, 20930821, 21947856, 22997593,
            24080695, 25259906, 26475754, 27728955, 29020233, 30350318, 31719944, 33129852, 34580790, 36073511, 37608773, 39270442, 40978509,
            42733789, 44537107, 46389292, 48291180, 50243611, 52247435, 54303504, 56412678, 58575823, 60793812, 63067521, 65397835, 67785643,
            70231841, 72737330, 75303019, 77929820, 80618654, 83370455, 86186124, 89066630, 92012904, 95025896, 98106559, 101255855, 104474750,
            107764216, 111125230, 114558777, 118065845, 121647430, 125304532, 129038159, 132849323, 136739041, 140708338, 144758242, 148889790,
            153104021, 157401983, 161784728, 166253312, 170808801, 175452262, 180184770, 185007406, 189921255, 19492740, 200000001
        ]
    },
    init: function () {
        var self = this;
        this.fn._this = self;

        //Make RSN text fit width
        var text = $("#userinformation").find("h2").text();
        var size = 50;
        while (self.fn.getWidthOfText(text, "Telex", size) > 200) {
            size -= 1;
        }
        $("#userinformation").find("h2").css("font-size", size);

        //Initialize badge information popup
        var $popup = $("#badgeInfo");
        $(".badge").mouseover(function () {
            var pos = $(this).position();
            var width = $(this).width();
            var height = $(this).height();

            $popup.css({
                display: 'block',
                top: pos.top + height,
                left: pos.left + (width / 2) - ($popup.width() / 2)
            });

            $(".badgeInfoBody").html("<h2>" + $(this).data("badge") + "</h2><p>" + $(this).data("text") + "</p>");

        }).mouseout(function () {
            $("#badgeInfo").css("display", "none");
        });


        //Initialize maps for sort buttons
        $("map").each(function () {
            var map = $(this).attr("name");

            $(this).find("area").each(function () {
                $(this).hover(function () {
                    if ($(this).attr("title") == "Sort Ascending") {
                        $("img[usemap=#" + map + "]").attr("src", "/_assets/images/updown-up.png");
                    } else {
                        $("img[usemap=#" + map + "]").attr("src", "/_assets/images/updown-down.png");
                    }
                }, function () {
                    if (map == "sortmap" + self.settings.sortCol) {
                        if (self.settings.sortOrder == 0) {
                            $("img[usemap=#" + map + "]").attr("src", "/_assets/images/updown-up.png");
                        } else {
                            $("img[usemap=#" + map + "]").attr("src", "/_assets/images/updown-down.png");
                        }
                    } else {
                        $("img[usemap=#" + map + "]").attr("src", "/_assets/images/updown.png");
                    }
                });
            });
        });
    },
    fn: {
        _this: this,
        sort: function (column, order) {
            var self = this._this;

            self.settings.sortCol = column;
            self.settings.sortOrder = order;

            $(".sortBtn").attr("src", "/_assets/images/updown.png");
            var img = $("img[usemap=#sortmap" + column + "]");

            if (order == 0) {
                img.attr("src", "/_assets/images/updown-up.png");
            } else {
                img.attr("src", "/_assets/images/updown-down.png");
            }
        },
        recalculate: function () {
            var self = this._this;
            $("#quickCalculateForm").find("input").each(function () {
                var skill = $(this).data("skill");
                var target = $(this).val();

                self.fn.recalculateSkill(skill, target);
            });

            self.fn.recalculateTotalRemainingXP();
        },
        recalculateSkill: function (skill, target) {
            var self = this._this;

            var skillRow = $("#skill-" + skill);
            var skillExp = parseInt(skillRow.find(".skillexp").text().replace(/,/g, ""), 10);
            var skillType = skillRow.data("skilltype");
            var maxRealLevel = parseInt(skillRow.data("maxlevel"), 10);
            var maxVirtualLevel = skillType == "Normal" ? 126 : 150;

            var targetCell = skillRow.find(".skilltarget");

            var xpScale = skillType == "Normal" ? self.xpScales.normal : self.xpScales.elite;
            var targetXP = 0;

            if (target <= maxVirtualLevel) {
                //Level target
                targetXP = self.fn.getExpFromLevel(target, xpScale);
                targetCell.find(".levelDisplay").text(target);
            } else {
                //XP target
                targetXP = target;
                targetCell.find(".levelDisplay").text("XP");
            }

            var remainingXP = targetXP - skillExp;
            if (remainingXP < 0) {
                remainingXP = 0;
            }

            targetCell.find(".xpDisplay").text(numberWithCommas(remainingXP));


            var percentageComplete = (skillExp / targetXP) * 100;
            if (percentageComplete > 100) {
                percentageComplete = 100;
            }

            skillRow.find(".skillpercentage").find(".skillbar_bar").animate({width: percentageComplete + "%"}, 500);
            skillRow.find(".skill_bar_percent").text(percentageComplete == 100 ? percentageComplete + "%" : percentageComplete.toFixed(2) + "%");
            skillRow.data("target", target);
        },
        recalculateTotalRemainingXP: function () {
            var self = this._this;
            var remainingXP = 0;
            var xpGoal = 0;

            $(".skillprogress").find(".skilltarget").each(function () {
                remainingXP += parseInt($(this).find(".xpDisplay").text().replace(/,/g, ""), 10);

                var skillRow = $(this).parent(".skillprogress");
                var skillType = skillRow.data("skilltype");
                var maxVirtualLevel = skillType == "Normal" ? 126 : 150;
                var target = parseInt(skillRow.data('target'), 10);
                var xpScale = skillType == "Normal" ? self.xpScales.normal : self.xpScales.elite;

                if (target < maxVirtualLevel) {
                    target = self.fn.getExpFromLevel(target, xpScale);
                }

                xpGoal += target;
            });

            var percentageComplete = ((xpGoal - remainingXP) / xpGoal);

            $(".xpremaining").find("p").text("XP Remaining: " + numberWithCommas(remainingXP));

            console.log(percentageComplete);
            $(".demo-1").circleProgress("value", percentageComplete);
            $("#avatar").find("p").text((percentageComplete * 100).toFixed(2) + "%");
        },
        getWidthOfText: function (txt, fontname, fontsize) {
            // Create a dummy canvas (render invisible with css)
            var c = document.createElement('canvas');
            // Get the context of the dummy canvas
            var ctx = c.getContext('2d');
            // Set the context.font to the font that you are using
            ctx.font = fontsize + 'px ' + fontname;
            // Measure the string

            return Math.ceil(ctx.measureText(txt).width);
        },
        getExpFromLevel: function (level, xpScale) {
            return xpScale[level - 1];
        },
        getLevelFromExp: function (exp, xpScale) {
            for (var i = 0; i <= xpScale.length; i++) {
                if (exp >= xpScale[i] && exp < xpScale[i + 1]) {
                    return i + 1;
                }
            }
            return 1;
        }
    }
};

calc.init();