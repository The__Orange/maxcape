var $popup = $("#badgeInfo");

$(".badge").mouseover(function () {
    var pos = $(this).position();
    var width = $(this).width();
    var height = $(this).height();

    $popup.css({
        display: 'block',
        top: pos.top + height,
        left: pos.left + (width/2) - ($popup.width()/2)
    });

    $(".badgeInfoBody").html("<h2>" + $(this).data("badge") + "</h2><p>" + $(this).data("text") + "</p>");

}).mouseout(function () {
    $("#badgeInfo").css("display", "none");
});