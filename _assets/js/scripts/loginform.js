$('.ddl_menu > li').bind('mouseover', openSubMenu).bind('mouseout', closeSubMenu);

$("#login_username, #login_password").blur(function () {
    var $dropdown = $(this).parent("div").parent("form").parent("div").parent("ul");

    if (!$dropdown.is(":hover")) {
        $dropdown.css("visibility", "hidden");
    }
});

function openSubMenu() {
    var ddl = $("#loginForm");
    ddl.css('display', 'block');

    var right = $(window).width() - ($(this).offset().left + $(this).outerWidth());
    var top = $(this).outerHeight();
    console.log(right);
    console.log(top);
    ddl.css("right", right + "px");
    ddl.css("top", top + "px");
}

function closeSubMenu() {
    if (!$("#login_username").is(":focus") && !$("#login_password").is(":focus")) {
        $("#loginForm").css('display', 'none');
    }
}