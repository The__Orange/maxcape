function lavalamp() {
    var active = $("nav").find(".active");
    var lavalamp = $("#lavalamp");

    var left = active.offset().left;
    var width = active.width();

    lavalamp.css("left", left + "px");
    lavalamp.css("width", width + "px");
}

function getWidthOfText(txt, fontname, fontsize){
    // Create a dummy canvas (render invisible with css)
    var c=document.createElement('canvas');
    // Get the context of the dummy canvas
    var ctx=c.getContext('2d');
    // Set the context.font to the font that you are using
    ctx.font = fontsize + 'px ' + fontname;
    // Measure the string

    return Math.ceil(ctx.measureText(txt).width);
}

function ellipses() {
    $("#recentForumPosts").find(".sidebarlist").find("li").each(function () {
        var $li = $(this),
            liwidth = $li.width(),
            $title = $li.find(".threadtitle").find("span").find("a"),
            title = $title.data('title'),
            titlewidth = $title.width();
        titlewidth += $li.find(".userportrait").width();
        var i = 0;
        var looping = true;
        var text, loop, span;
        do {
            text = title;
            loop = $title.data('loop') + 1;

            if (text.indexOf("...") != -1) {
                text = text.slice(0, text.length - (3 * loop));
            }
            text = text.slice(0, text.length - (3 * loop)) + "...";

            titlewidth = getWidthOfText(text, "Telex", "17")+ $li.find(".userportrait").width();
            i++;
            $title.data('loop', i);
            if (titlewidth <= liwidth || i >= 25) {
                $title.text(text);
                looping = false;
            }
        } while (looping);
    });
}

$(document).ready(function () {
    $(window).load(function () {
        lavalamp();

        //dirty hack to add the class ONLY AFTER the lavalamp has been positioned
        window.setTimeout(function () {
            $("#lavalamp").addClass("slider")
        }, 1);

        $(".newsItem").each(function () {
            var images = $(this).find("img:not(.avatar)").clone();
            $(this).find("img:not(.avatar)").remove();
            $(this).find("h3:first-of-type").after(images);
        });

        ellipses();
    });



    $(window).scroll(function () {
        if ($(window).scrollTop() > 60) {
            $("nav").addClass("fixed");
            $("#header").addClass("fixedNav");
            $("#mainContent, #sidebar").addClass("fixedNavMargin");
        } else {
            $("nav").removeClass("fixed");
            $("#header").removeClass("fixedNav");
            $("#mainContent, #sidebar").removeClass("fixedNavMargin");
        }
    });

    $(window).resize(function () {
        lavalamp();
        ellipses();
    });



    $('.socialicon').click(function (e) {
        e.preventDefault();

        var width = 575,
            height = 400,
            left = ($(window).width() - width) / 2,
            top = ($(window).height() - height) / 2,
            opts = 'status=1' +
                ',width=' + width +
                ',height=' + height +
                ',top=' + top +
                ',left=' + left;

        var url = "";
        var urls = {
            "twitter": "http://twitter.com/share?url={url}&via=The__Orange&hashtags=RuneScape&text={text}",
            "facebook": "http://www.facebook.com/sharer.php?s=100&p[title]={title}&p[summary]={text}&p[url]={url}",
            "tumblr": "http://www.tumblr.com/share/link?url={url2}&name={title}&description={text}",
            "googleplus": "https://plus.google.com/share?u{url}"
        };

        for (var i = 0; i < Object.keys(urls).length; i++) {
            console.log(i);
            if ($(this).hasClass(Object.keys(urls)[i])) {
                url = urls[Object.keys(urls)[i]];
                break;
            }
        }

        url = url.replace("{url}", location.href);
        url = url.replace("{url2}", location.href.replace("http://", ""));
        url = url.replace("{text}", document.title);
        url = url.replace("{title}", document.title);

        url = encodeURI(url);

        window.open(url, 'Share to Social Media', opts);

        return false;
    });


    $("#calcSearch").click(function () {
        var toggled = $(this).data("expanded");
        $(this).find("a").css("display", toggled ? "block" : "none");
        if (toggled) {
            $("#calcSearchBox").css("width", "0");
        } else {
            $("#calcSearchBox").css("width", "125px").find("input").focus();
        }

        $(this).data("expanded", !toggled);
    });

    $("#calcSearchBox")
        .click(function (e) {
            e.stopPropagation();
        })
        .find("input")
        .blur(function () {
            $("#calcSearchBox").css("width", "0");
            $("#calcSearch").data("expanded", false).find("a").css("display", "block");
            lavalamp();
        })
        .on('input', function () {
            var regex = /^([a-zA-Z0-9\-_ ]{1,12})$/g;

            var val = $(this).val();
            if (!regex.test(val)) {
                val = val.substring(0, val.length - 1);
                $(this).val(val);
            }
        });

    $("nav li").mouseover(function () {
        var lavalamp = $("#lavalamp");
        var left = $(this).offset().left;
        var width = $(this).width();

        lavalamp.css("left", left + "px");
        lavalamp.css("width", width + "px");
    });

    $("nav").mouseleave(function () {
        var active = $(this).find(".active");
        var lavalamp = $("#lavalamp");

        var left = active.offset().left;
        var width = active.width();

        lavalamp.css("left", left + "px");
        lavalamp.css("width", width + "px");
    });

    $("#timer-container").click(function () {
        var toggled = $(this).data("toggled");
        console.log(toggled);

        if (!toggled) {
            $("#timer-toggle-container").animate({
                width: '35px',
                marginLeft: '5px'
            }, 250);
        } else {
            $("#timer-toggle-container").animate({
                width: 0,
                marginLeft: 0
            }, 250);
        }

        $(this).data("toggled", !toggled);
    });

    $(".rslogo").click(function() {
        $(".rslogoactive").removeClass("rslogoactive");
        $(this).addClass("rslogoactive");
    });

    $("#closeAlert").click(function() {
        $(this).parent(".errorMessage").slideUp();
    });
});

