var $useropts = $("#userdisplay");

function openSubMenu() {
    var ddl = $useropts.find("ul");
    ddl.css('display', 'block');
    ddl.css('visibility', "visible");

    var right = $(window).width() - ($(this).offset().left + $(this).outerWidth());
    var top = $(this).outerHeight();
    ddl.css("right", right + "px");
    ddl.css("top", top + "px");
}

function closeSubMenu() {
    $useropts.find("ul").css('display', 'none');
}

$('.ddl_menu > li').bind('mouseover', openSubMenu).bind('mouseout', closeSubMenu);