var sortCol = 0;
var sortOrder = 1;

$("map").each(function() {
    var map = $(this).attr("name");

    $(this).find("area").each(function() {
        $(this).hover(function() {
            if($(this).attr("title") == "Sort Ascending") {
                $("img[usemap=#" + map + "]").attr("src", "/_assets/images/updown-up.png");
            } else {
                $("img[usemap=#" + map + "]").attr("src", "/_assets/images/updown-down.png");
            }
        }, function() {
            if(map == "sortmap" + sortCol) {
                if(sortOrder == 0) {
                    $("img[usemap=#" + map + "]").attr("src", "/_assets/images/updown-up.png");
                } else {
                    $("img[usemap=#" + map + "]").attr("src", "/_assets/images/updown-down.png");
                }
            } else {
                $("img[usemap=#" + map + "]").attr("src", "/_assets/images/updown.png");
            }
        });
    });
});

function sort(column, order) {
    sortCol = column;
    sortOrder = order;

    $(".sortBtn").attr("src", "/_assets/images/updown.png");
    var img = $("img[usemap=#sortmap" + sortCol + "]");

    console.log(img);

    if(sortOrder == 0) {
        img.attr("src", "/_assets/images/updown-up.png");
    } else {
        img.attr("src", "/_assets/images/updown-down.png");
    }


}